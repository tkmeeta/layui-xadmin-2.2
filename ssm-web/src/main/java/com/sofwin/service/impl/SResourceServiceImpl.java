package com.sofwin.service.impl;

import com.sofwin.dto.SResourceDto;
import com.sofwin.mapper.SResourceMapper;
import com.sofwin.pojo.SResource;
import com.sofwin.pojo.SResourceKey;
import com.sofwin.service.SResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SResourceServiceImpl implements SResourceService {
    @Autowired
    private SResourceMapper resourceMapper;


    @Override
    public List<SResourceDto> selectAllResourceToTree() {
        return resourceMapper.selectAllResourceToList();
    }

    @Override
    public List<SResource> selectAllResourceToTree2() {
        return resourceMapper.selectAllResourceToList2();
    }

    @Override
    public boolean save(SResource sResource) {
        // 判断是修改还是新增
        if (sResource.getId()!=null){
            return resourceMapper.updateByPrimaryKeySelective(sResource)==1?true:false;
        }
        return resourceMapper.insertSelective(sResource)==1?true:false;
    }

    @Override
    public SResource selectByIdForTree(Integer id) {
        /*SResourceKey key = new SResourceKey();
        key.setId(id);
        return resourceMapper.selectByPrimaryKey(key);*/
        return resourceMapper.selectByTreeId(id);
    }

    @Override
    public boolean deleteNode(Integer id) {
        SResourceKey key = new SResourceKey();
        key.setId(id);
        return resourceMapper.deleteByPrimaryKey(key)==1?true:false;
    }

    @Override
    public List<SResource> selectAllResourceToTreeByRoleId(Integer roleId) {

        return resourceMapper.selectAllResourceToTreeByRoleId(roleId);
    }

    @Override
    public List<SResourceDto> selectRoleResource(Integer roleId) {
        return resourceMapper.selectRoleResource(roleId);
    }
}
