package com.sofwin.service.impl;

import com.sofwin.mapper.SDeptMapper;
import com.sofwin.pojo.SDept;
import com.sofwin.service.SDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SDeptServiceImpl implements SDeptService {
    @Autowired
    private SDeptMapper mapper;
    @Override
    public List<SDept> getDeptNames() {
        return mapper.selectByExample(null);
    }
}
