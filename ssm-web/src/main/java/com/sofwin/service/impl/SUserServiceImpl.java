package com.sofwin.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sofwin.dto.SUserDto;
import com.sofwin.mapper.SUserMapper;
import com.sofwin.pojo.SUser;
import com.sofwin.pojo.SUserExample;
import com.sofwin.pojo.SUserKey;
import com.sofwin.service.SUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("userService")
public class SUserServiceImpl implements SUserService {
    @Autowired
    private SUserMapper mapper;
    @Override
    /*@Transactional(propagation = Propagation.REQUIRED)*/
    public boolean saveUser(SUser sUser) {
        boolean flag = false;
        int i = mapper.insertSelective(sUser);
        if (i==1){
            /*int i1 = 1/0;*/
            flag = true;
            return flag;
        }
        return false;
    }

    @Override
    public boolean checkLogin(SUser user) {
        boolean isExist = false;
        SUser sUser = selectUser(user);
        if (sUser!=null){
            isExist=true;
        }
        return isExist;
    }

    @Override
    public SUser selectUser(SUser user) {
        SUserExample example = new SUserExample();
        SUserExample.Criteria criteria = example.createCriteria();
        if (user!=null){
            if (user.getLoginAccount()!=null && !"".equals(user.getLoginAccount())){
                // 追加where的条件
                criteria.andLoginAccountEqualTo(user.getLoginAccount());
            }
            if (user.getPwd()!=null && !"".equals(user.getPwd())){
                criteria.andPwdEqualTo(user.getPwd());
            }
        }
        // 把自己创建的查询条件-传参
        List<SUser> sUsers = mapper.selectByExample(example);
        // 根据前端登录的条件查询的用户-是否存在在数据库中
        if (sUsers!=null && sUsers.size()>0){
            return sUsers.get(0);
        }
        return null;
    }

    @Override
    public PageInfo<SUserDto> selectUsers(SUser user, Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber,pageSize);
        List<SUserDto> sUsers = mapper.selectUsers(user);
        return new PageInfo<SUserDto>(sUsers);
    }

    @Override
    public boolean deletesByIds(Integer[] ids) {
        int flag = 0;
        for (Integer id:ids){
            SUserKey s = new SUserKey();
            s.setId(id);
            flag+=mapper.deleteByPrimaryKey(s);
        }
        return flag==ids.length?true:false;
    }

    @Override
    public boolean insertUser(SUser user) {
        if (user.getId() != null){
            // 修改
            return mapper.updateByPrimaryKeySelective(user)==1?true:false;
        }
        return mapper.insertSelective(user)==1?true:false;
    }

    @Override
    public SUser selectUserById(Integer id) {
        SUserKey key = new SUserKey();
        key.setId(id);
        SUser user = mapper.selectByPrimaryKey(key);
        return user;

    }

    @Override
    public boolean selectUserByLoginAccount(String username) {
        SUserExample example = new SUserExample();
        SUserExample.Criteria criteria = example.createCriteria();
        criteria.andLoginAccountEqualTo(username);
        List<SUser> sUsers = mapper.selectByExample(example);
        if (sUsers != null && sUsers.size()==1){
            return true;
        }
        return false;
    }

    @Override
    public List<String> selectUserPermission(Integer roleId) {
        return mapper.selectUserPermission(roleId);
    }
}
