package com.sofwin.service;

import com.sofwin.pojo.SDept;

import java.util.List;

public interface SDeptService {
    /**
     * 获取部门名称
     * @return
     */
    List<SDept> getDeptNames();
}
