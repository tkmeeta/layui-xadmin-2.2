package com.sofwin.service;

import com.sofwin.dto.SResourceDto;
import com.sofwin.pojo.SResource;

import java.util.List;

public interface SResourceService {
    /**
     * 查找资源关系显示树状
     * @return
     */
    List<SResourceDto> selectAllResourceToTree();

    /**
     * z-tree 简单数据类型 三级目录结构
     * @return
     */
    List<SResource> selectAllResourceToTree2();

    /**
     * 添加|修改 资源到数据库
     * @param sResource
     * @return
     */
    boolean save(SResource sResource);

    /**
     * 点击树节点传来ID，根据id查询数据库 以 回显
     * @param id
     * @return
     */
    SResource selectByIdForTree(Integer id);

    /**
     * 删除节点
     * @param id
     * @return
     */
    boolean deleteNode(Integer id);

    /**
     * 点击设置，根据角色id回显角色可用的资源状态
     * @param roleId
     * @return
     */
    //List<SResourceDto> selectAllResourceToTreeByRoleId(Integer roleId);
    List<SResource> selectAllResourceToTreeByRoleId(Integer roleId);

    /**
     * 根据 角色 动态展示主页的可用资源
     * @param roleId
     * @return
     */
    List<SResourceDto> selectRoleResource(Integer roleId);
}
