package com.sofwin.service;

import com.github.pagehelper.PageInfo;
import com.sofwin.dto.SUserDto;
import com.sofwin.pojo.SUser;

import java.util.List;

public interface SUserService {
    /**
     * 增加用户
     * @param sUser
     * @return 是否成功 true：成功 false：失败
     */
    boolean saveUser(SUser sUser);

    /**
     * 登录
     * 验证用户名+密码
     * @param user
     * @return 数据库中是否有
     */
    boolean checkLogin(SUser user);

    /**
     * 登录
     * 根据自定义条件 查询用户信息
     * @param user
     * @return
     */
    SUser selectUser(SUser user);

    /**
     * 查询
     * 关联查询用户的相关信息
     * @return
     */
    PageInfo<SUserDto> selectUsers(SUser user, Integer pageNumber, Integer pageSize);

    /**
     * 删除
     * 根据选中的批量删除
     * @param ids
     * @return
     */
    boolean deletesByIds(Integer[] ids);

    /**
     * 新增用户信息
     * @param user
     * @return
     */
    boolean insertUser(SUser user);

    /**
     * 根据编辑按钮的id，查询用户信息，以回显。
     * @param id
     * @return
     */
    SUser selectUserById(Integer id);

    /**
     * 检查是否存在 该 用户名
     * @param username
     * @return
     */
    boolean selectUserByLoginAccount(String username);

    /**
     * 获取 当前用户对应角色 的细粒度权限
     * @param roleId
     * @return
     */
    List<String> selectUserPermission(Integer roleId);

}
