package com.sofwin.service;

import com.github.pagehelper.PageInfo;
import com.sofwin.pojo.SRole;

import java.util.List;

public interface SRoleService {
    /**
     * 获取角色名称
     * @return
     */
    List<SRole> getRoleNames();

    /**
     * 分页查询角色表
     * @param role
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<SRole> selectRoleByPage(SRole role,  Integer pageNum, Integer pageSize);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean deleteRoles(Integer[] ids);

    /**
     * 编辑角色信息，以修改
     * @param id
     * @return
     */
    SRole selectById(Integer id);

    /**
     * 新增角色信息
     * @param role
     * @return
     */
    boolean insertRole(SRole role);

    /**
     * 设置用户的资源权限
     * @param isChecked 勾选|取消勾选
     * @param roleId
     * @param resourceId
     * @return
     */
    //boolean savePermission(boolean isChecked, Integer roleId, Integer resourceId);
    boolean savePermission1(Integer roleId, Integer[] resourceIds);
}
