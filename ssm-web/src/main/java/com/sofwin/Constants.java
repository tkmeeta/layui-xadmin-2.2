package com.sofwin;

/**
 * 常量
 */
public class Constants {

    // 当前用户
    public static final String CURR_USER="curruser";

    // 每页显示的条数
    public static final String DEFAULT_PAGE_SIZE="15";

    // 图形验证码尾巴上的session
    //public static final String KAPTCHA_SESSION_KEY="kaptchaSession";
}
