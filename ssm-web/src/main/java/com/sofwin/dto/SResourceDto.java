package com.sofwin.dto;

import com.sofwin.pojo.SResource;
import lombok.Data;
/**
 * 映射资源树的
 */
import java.util.List;
@Data
public class SResourceDto extends SResource {

    public boolean open = true;

    public List<SResource> children;
}
