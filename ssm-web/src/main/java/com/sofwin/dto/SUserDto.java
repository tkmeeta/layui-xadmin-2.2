package com.sofwin.dto;

import com.sofwin.pojo.SDept;
import com.sofwin.pojo.SRole;
import com.sofwin.pojo.SUser;
import lombok.Data;

/**
 * 查询用户信息 扩展类
 */
@Data
public class SUserDto extends SUser {

    private SDept dept;
    private SRole role;


}
