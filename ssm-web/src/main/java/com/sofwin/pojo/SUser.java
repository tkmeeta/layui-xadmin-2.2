package com.sofwin.pojo;

import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class SUser extends SUserKey implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.login_account
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private String loginAccount;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.pwd
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private String pwd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.real_name
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private String realName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.flag
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private Integer flag;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.last_login
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private String lastLogin;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.dept_id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private Integer deptId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_user.role_id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private Integer roleId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.login_account
     *
     * @return the value of s_user.login_account
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public String getLoginAccount() {
        return loginAccount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.login_account
     *
     * @param loginAccount the value for s_user.login_account
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount == null ? null : loginAccount.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.pwd
     *
     * @return the value of s_user.pwd
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.pwd
     *
     * @param pwd the value for s_user.pwd
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.real_name
     *
     * @return the value of s_user.real_name
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public String getRealName() {
        return realName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.real_name
     *
     * @param realName the value for s_user.real_name
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.flag
     *
     * @return the value of s_user.flag
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public Integer getFlag() {
        return flag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.flag
     *
     * @param flag the value for s_user.flag
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.last_login
     *
     * @return the value of s_user.last_login
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public String getLastLogin() {
        return lastLogin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.last_login
     *
     * @param lastLogin the value for s_user.last_login
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin == null ? null : lastLogin.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.dept_id
     *
     * @return the value of s_user.dept_id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public Integer getDeptId() {
        return deptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.dept_id
     *
     * @param deptId the value for s_user.dept_id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_user.role_id
     *
     * @return the value of s_user.role_id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_user.role_id
     *
     * @param roleId the value for s_user.role_id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}