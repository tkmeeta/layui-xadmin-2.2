package com.sofwin.pojo;

public class SRoleKey {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_role.id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    private Integer id;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_role.id
     *
     * @return the value of s_role.id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_role.id
     *
     * @param id the value for s_role.id
     *
     * @mbg.generated Thu Mar 25 19:37:23 CST 2021
     */
    public void setId(Integer id) {
        this.id = id;
    }
}