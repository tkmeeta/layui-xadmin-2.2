<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/26
  Time: 下午 9:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<% String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>用户管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="layui/css/xadmin.css">
    <script src="layui/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="layui/js/xadmin.js"></script>
</head>
<body>
<div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">演示</a>
            <a>
              <cite>导航元素</cite></a>
          </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
</div>
<div class="layui-fluid layui-form">
    <form class="layui-form layui-col-space5" action="/system/user/data" id="form" method="post">
        <input type="hidden" name="pageNumber" value="${page.pageNum}" id="pageNumber">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body ">
                        <div class="layui-inline layui-show-xs-block">
                            <input type="text" name="loginAccount" value="${user.loginAccount}" placeholder="请输入用户名" autocomplete="off" class="layui-input">
                        </div>
                        <shiro:hasPermission name="user:query">
                        <div class="layui-inline layui-show-xs-block">
                            <button class="layui-btn search_btn" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                        </div>
                        </shiro:hasPermission>
                        <div class="layui-card-header layui-inline">
                            <shiro:hasPermission name="user:deletes">
                            <button class="layui-btn layui-btn-danger batchDel" type="button" ><i class="layui-icon"></i>批量删除</button>
                            </shiro:hasPermission>
                            <shiro:hasPermission name="user:add">
                            <button class="layui-btn usersAdd_btn" type="button" ><i class="layui-icon"></i>添加</button>
                            </shiro:hasPermission>
                        </div>
                </div>
                <div class="layui-card-body layui-table-body layui-table-main">
                    <table class="layui-table layui-form">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="allChoose" lay-filter="allChoose" lay-skin="primary">
                            </th>
                            <th>登录名</th>
                            <th>姓名</th>
                            <th>所属部门</th>
                            <th>所属角色</th>
                            <th>状态</th>
                            <th>最后登录时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%--循环数据--%>
                        <c:choose>
                            <c:when test="${fn.length(data)==0}">
                                <tr>
                                    <td colspan="8">暂无数据</td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${data}" var="user" varStatus="v">
                                    <tr>
                                        <td><input type="checkbox" name="ids" lay-skin="primary" lay-filter="aChoose" value="${user.id}" id="delete_${user.id}"></td>
                                        <td>${user.loginAccount}</td>
                                        <td>${user.realName}</td>
                                        <td>${user.dept.deptName}</td>
                                        <td>${user.role.roleName}</td>
                                        <td>
                                            <input type="checkbox" lay-filter="OO" lay-skin="switch" data="${user.id}" lay-text="启用|停用" <c:if test="${user.flag==1}">checked</c:if> />
                                        </td>
                                        <td>${user.lastLogin}</td>
                                        <td>
                                            <shiro:hasPermission name="user:edit">
                                            <a title="编辑" href="javascript:void(0)" class="edit" data="${user.id}">
                                                <i class="layui-icon">&#xe642;</i>
                                            </a>
                                            </shiro:hasPermission>
                                            <shiro:hasPermission name="user:delete">&nbsp;&nbsp;
                                            <a href="javascript:void(0)" title="删除" class="delete" dataId="${user.id}" >
                                                <i class="layui-icon">&#xe640;</i>
                                            </a>
                                            </shiro:hasPermission>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
                <%--页码--%>
                    <div id="page"></div>
            </div>
        </div>
    </div>
</form>
</div>
</body>
<script>
    layui.use(['laydate','form','jquery','layer','laypage','tree'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var $ = layui.jquery;
        var layer = layui.layer;
        var laypage = layui.laypage;
        // 监听全选
        form.on('checkbox(allChoose)', function(data){
            $("input[name='ids']").each(function () {
                this.checked = data.elem.checked;
            });
            form.render();
        });
        // 监听单选
        form.on('checkbox(aChoose)',function (data) {
            // 多选框的个数
            var allIds = $("input[name='ids']").length;
            // 被选中的多选框的长度
            var allIdsChecked = $("input[name='ids']:checked").length;
            if (parseInt(allIdsChecked)<allIds){
                // 不是全选中，取消√
                $("#allChoose").each(function () {
                    this.checked = data.elem.checked;
                })
            }else {
                // 是全选中，再√上
                $("#allChoose").each(function () {
                    this.checked = data.elem.checked;
                })
            }
            form.render();
        })
        // 点击添加用户
        $(".usersAdd_btn").click(function () {
            layer.open({
                title:'添加用户信息',
                type:2,
                content:'system/user/toEdit',
                area:['500px','480px'],
            });
        });

        // 分页
        laypage.render({
            elem:'page',
            count:parseInt(${page.total}),
            limit:parseInt(${page.pageSize}), // 每页显示的条数
            curr:parseInt(${page.pageNum}),
            layout:['prev', 'page', 'next','skip'],
            jump:function (obj,first) {
                if (!first){
                    // 不是第一次加载，说明被点击了
                    $("#pageNumber").val(obj.curr);
                    $("#form").submit();
                }
            }
        });

        // 模糊查询
        $(".search_btn").click(function () {
            $("#form").submit();
        });

        // 批量删除
        $(".batchDel").click(function () {
            if($("input[name='ids']:checked").length==0){
                layer.msg("你先选几个再点我删除么");
                return;
            }
            layer.confirm("是否确定删除"+$("input[name='ids']:checked").length+"个用户？",{icon:2,title:'Tip'},function () {
                var idsParam = $("#form").serialize();
                $.ajax({
                    url:'system/user/deletes',
                    type:'post',
                    dataType:'json',
                    data:idsParam,
                    success:function(ret){
                        if (ret.status){
                            layer.msg("删除成功！",{
                                offset:"t",
                                time:2000,
                                anim:4
                            },function () {
                                $("#form").submit();
                            })
                        }
                    }
                })
            })
        })

        // 单行删除
        $(".delete").click(function () {
            // var id = $(this).attr("dataId");
            // // 把选中的全 反选
            // $("input[name=ids]:checked").each(function () {
            //     $(this).removeAttr("checked");
            // })
            // // 选中当行
            // $("#delete_"+id)[0].checked=true;
            // form.render();
            // // 调用批量删除方法
            // $(".batchDel").click();

            var id = $(this).attr("dataId");
            layer.confirm("是否确定删除这个用户？",{icon:1,title:'Tip'},function () {
                $.ajax({
                    url:'system/user/deletes',
                    type:'post',
                    dataType:'json',
                    data:{
                        ids:id
                    },
                    success:function(ret){
                        if (ret.status){
                            layer.msg("删除成功！",{
                                offset:"t",
                                time:2000,
                                anim:4
                            },function () {
                                $("#form").submit();
                            })
                        }
                    }
                })
            })

        });

        // 编辑,打开layer层
        $(".edit").click(function () {
            let id = $(this).attr("data");
            index = layer.open({
                title:'修改用户信息',
                type:2,
                content:'system/user/toEdit?id='+id,
                area:['500px','480px']
            });
        });

        // 状态改变
        form.on('switch(OO)',function (obj) {
            let id = $(this).attr("data");
            let flag = 0;
            if (obj.elem.checked){
                flag = 1;
            }
            $.ajax({
                url:'system/user/save?id='+id+'&flag='+flag,
                type:'get',
                dataType:'json',
                success:function (ret) {
                    if (!ret.status){
                        layer.msg("操作失败！")
                    }
                }
            })
        });

        layui.tree({
            elem: '#demo' //传入元素选择器
            ,nodes: [{ //节点
                name: '父节点1'
                ,children: [{
                    name: '子节点11'
                },{
                    name: '子节点12'
                }]
            },{
                name: '父节点2（可以点左侧箭头，也可以双击标题）'
                ,children: [{
                    name: '子节点21'
                    ,children: [{
                        name: '子节点211'
                    }]
                }]
            }]
        });


    });


</script>
</html>
