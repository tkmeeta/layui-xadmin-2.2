<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/28
  Time: 下午 1:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path=request.getContextPath()+"/";%>

<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="layui/css/xadmin.css">
    <script type="text/javascript" src="layui/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="layui/js/xadmin.js"></script>
</head>
<body>
<div class="layui-fluid layui-form">
    <div class="layui-row">
        <form class="layui-form" action="/system/role/save" method="post" id="form">
            <input type="hidden" name="id" value="${user.id}">
            <div class="layui-form-item">
                <label class="layui-form-label"><span class="x-red">*</span>登录名</label>
                <div class="layui-input-block">
                    <input type="text" name="loginAccount" value="${user.loginAccount}" class="layui-input userName" lay-verify="nikename" required placeholder="请输入登录名">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"><span class="x-red">*</span>密码</label>
                <div class="layui-input-block">
                    <input type="text" name="pwd" value="${user.pwd}" class="layui-input" lay-verify="pass" required placeholder="请输入密码">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"><span class="x-red">*</span>姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="realName" value="${user.realName}" class="layui-input userEmail" lay-verify="required" placeholder="请输入真实姓名">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label"><span class="x-red">*</span>所属部门</label>
                    <div class="layui-input-block">
                        <select name="deptId" class="userGrade" lay-filter="userGrade">
                            <c:forEach items="${depts}" var="dept" varStatus="v">
                                <option value="${dept.id}" <c:if test="${user.deptId==dept.id}">selected</c:if> >${dept.deptName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label"><span class="x-red">*</span>所属角色</label>
                    <div class="layui-input-block">
                        <select name="roleId" class="userGrade" lay-filter="userGrade">
                            <c:forEach items="${roles}" var="role" varStatus="v">
                                <option value="${role.id}" <c:if test="${user.roleId==role.id}">selected</c:if> >${role.roleName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <input type="checkbox" lay-filter="oo" lay-skin="switch" lay-text="启用|禁用" <c:if test="${user.flag==1}">checked</c:if> >
                        <input type="hidden" id="flag" name="flag" <c:if test="${empty user}">value="0"</c:if> <c:if test="${not empty user}">value="${user.flag}"</c:if>>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" type="button" id="save" lay-filter="addUser">立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>layui.use(['form', 'layer','jquery'],
    function() {
        $ = layui.jquery;
        var form = layui.form,
            layer = layui.layer;

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 5) {
                    return '昵称至少得5个字符啊';
                }
            },
            pass: [/(.+){6,12}$/, '密码必须6到12位'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });

        //监听提交
        // 监听-状态开关 ?1、不手动点击就监听不到
        form.on('switch(oo)',function (data) {
            // console.log(data.elem); //得到checkbox原始DOM对象
            // console.log(data.elem.checked); //开关是否开启，true或者false
            // console.log(data.value); //开关value值，也可以通过data.elem.value得到
            // console.log(data.othis); //得到美化后的DOM对象
            var checked = data.elem.checked;
            if (checked){
                $("#flag").val(1);
            }else {
                $("#flag").val(0);
            }
        });

        // 插入新增用户|
        $("#save").click(function () {
            let param = $("#form").serialize();
            $.ajax({
                url:'system/user/save',
                type:'get',
                dataType:'json',
                data:param,
                success:function (ret) {
                    if (ret.status){
                        layer.msg("操作成功！",function () {
                            // 关闭弹窗
                            let index = parent.layer.getFrameIndex(window.name);
                            layer.close(index);
                            // 父页面刷新
                            parent.document.location='system/user/data';
                        })
                    }
                }
            })
        });

        // 渲染
        form.render();

    });
</script>
</body>

</html>