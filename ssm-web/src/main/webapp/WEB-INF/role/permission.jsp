<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/28
  Time: 下午 1:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%String path=request.getContextPath()+"/";%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>修改角色权限</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="z-tree/css/demo.css" type="text/css">
    <link rel="stylesheet" href="layui/css/xadmin.css">
    <link rel="stylesheet" href="z-tree/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script type="text/javascript" src="z-tree/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="z-tree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="z-tree/js/jquery.ztree.exedit.js"></script>
    <script type="text/javascript" src="z-tree/js/jquery.ztree.excheck.js"></script>
    <script type="text/javascript" src="layui/lib/layui/layui.js" charset="utf-8"></script>
    <%--
    影响树的init方法
    <script type="text/javascript" src="layui/js/xadmin.js"></script>--%>
</head>
<body>
<div id="treeDemo" class="ztree"></div>

<script type="text/javascript">
    layui.use(['form','layer'],function () {
        form = layui.form;
        layer = layui.layer;

        var zTree;

        var setting = {
            data:{
              simpleData:{
                  enable:true,
                  idKey:"id",
                  pIdKey:"pid",
                  rootPId:0
              }
            },
            edit:{
                // 是否可处于编辑状态
                enable:false,
            },
            check:{
                // 是否显示节点的复选框
                enable: true
            },
            callback:{
                // 捕获复选框的勾选|取消勾选 事件的回调函数
                onCheck:zTreeOnCheck1
            }
        };
        // ======================================复选框勾选事件============================================
        function zTreeOnCheck(event, treeId, treeNode){
            //alert(JSON.stringify(treeNode))
            // 1、判断 勾选|取消 事件
            let isChecked = treeNode.checked;
            let roleId = '${roleId}';
            let resourceId = treeNode.id;
            // 2、异步请求
            $.ajax({
                url:"system/role/editPermission",
                data:{
                    isChecked:isChecked,
                    resourceId:resourceId,
                    roleId:roleId
                },
                type:'post',
                dataType:'json',
                success:function (ret) {
                    if (!ret.status){
                        layer.msg("设置失败");
                    }else{
                        layer.msg("设置成功！")
                    }
                }
            })
        }
        // =====================简单JSON数据 插入 ========================
        function zTreeOnCheck1(event,treeId,treeNode){
            var roleId = ${roleId};
            var url = "system/role/editPermission?roleId="+roleId;
            // 1、获取勾选中的节点 事件
            var checkedNodes = zTree.getCheckedNodes(true);
            // 2、循环添加到url尾巴上
            if (checkedNodes.length > 0){
                for (let i = 0; i < checkedNodes.length; i++) {
                    url += "&resourceIds="+checkedNodes[i].id;
                }
            }
            // 3、只能发get请求
            $.ajax({
                url:url,
                dataType:"json",
                type:"get",
                success:function (ret) {
                    if (!ret.status){
                        layer.msg("设置失败");
                    }else{
                        layer.msg("设置成功！")
                    }
                }
            })
        }

        // 生成树:带勾选状态回显
        $(document).ready(function () {
            $.getJSON('system/resource/createTree?flag=1&roleId=${roleId}&t='+new Date(),function (ret) {
                zTree = $.fn.zTree.init($("#treeDemo"),setting,ret); //位置，配置，json数据
                zTree.expandAll(true);
            })
        })



    })
</script>
</body>
</html>
