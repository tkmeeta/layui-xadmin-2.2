<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/28
  Time: 下午 1:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%String path=request.getContextPath()+"/";%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="layui/css/xadmin.css">
    <script type="text/javascript" src="layui/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="layui/js/xadmin.js"></script>
</head>
<body>
<div class="layui-fluid layui-form">
    <div class="layui-row">
        <form class="layui-form" action="/system/role/save" method="post" id="form">
            <div class="layui-form-item">
                <label class="layui-form-label">角色名称</label>
                <div class="layui-input-block">
                    <input type="hidden" name="id" value="${role.id}">
                    <input type="text" name="roleName" value="${role.roleName}" class="layui-input userName" lay-verify="required" placeholder="请输入角色名称">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-filter="oo" lay-skin="switch" lay-text="开启|关闭" <c:if test="${role.flag==1}">checked</c:if> >
                    <input type="hidden" id="flag" name="flag" <c:if test="${empty role}">value="0"</c:if> <c:if test="${not empty role}">value="${role.flag}"</c:if>  >
                </div>
            </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" type="button" id="save" lay-filter="addUser">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    layui.use(['form','jquery','layer'],function () {
        form = layui.form;
        $ = layui.jquery;
        layer = layui.layer;

        // 开关监听
        form.on('switch(oo)',function (data) {
            // console.log(data.elem); //得到checkbox原始DOM对象
            // console.log(data.elem.checked); //开关是否开启，true或者false
            // console.log(data.value); //开关value值，也可以通过data.elem.value得到
            // console.log(data.othis); //得到美化后的DOM对象
            if (data.elem.checked){
                $("#flag").val(1);
            }else {
                $("#flag").val(0);
            }
        });

        // 插入新增用户|
        $("#save").click(function () {
            let param = $("#form").serialize();
            $.ajax({
                url:'system/role/save',
                type:'get',
                dataType:'json',
                data:param,
                success:function (ret) {
                    if (ret.status){
                        layer.msg("修改成功！",function () {
                            // 关闭弹窗
                            let index = parent.layer.getFrameIndex(window.name);
                            layer.close(index);
                            // 父页面刷新
                            parent.document.location='system/role/data';
                        })
                    }
                }
            })
        });
        form.render();
    })
</script>
</body>
</html>
