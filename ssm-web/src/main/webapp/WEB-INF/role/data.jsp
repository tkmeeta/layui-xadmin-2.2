<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/26
  Time: 下午 9:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>用户管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="layui/css/xadmin.css">
    <script src="layui/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="layui/js/xadmin.js"></script>
</head>
<div>
<div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">演示</a>
            <a>
              <cite>导航元素</cite></a>
          </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
</div></div>
<div class="layui-fluid layui-form">
    <form class="layui-form layui-col-space5" action="/system/role/data" id="form" method="post">
        <input type="hidden" id="pageNum" name="pageNum" value="${page.pageNum}">
        <div class="layui-card">
            <div class="layui-card-body ">
                <div class="layui-inline layui-show-xs-block">
                    <input type="text" name="roleName" value="${role.roleName}" placeholder="角色名称" autocomplete="off" class="layui-input search_input">
                </div>
                <div class="layui-inline layui-show-xs-block">
                    <input type="text" name="flag" value="${role.flag}" placeholder="角色状态" autocomplete="off" class="layui-input search_input">
                </div>
                <div class="layui-inline layui-show-xs-block">
                    <button class="layui-btn search_btn" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                </div>
                <div class="layui-card-header layui-inline">
                    <button class="layui-btn layui-btn-danger batchDel" type="button" ><i class="layui-icon"></i>批量删除</button>
                    <button class="layui-btn usersAdd_btn" type="button" ><i class="layui-icon"></i>添加角色</button>
                </div>
            </div>
        <div class="layui-form users_list">
            <table class="layui-table">
                <colgroup>
                    <%--固定值 --%>
                    <col width="50">
                    <%--剩余的--%>
                    <col>
                    <%--百分比--%>
                    <col width="35%">
                    <col width="15%">
                    <col width="30%">
                </colgroup>
                <thead>
                <tr>
                    <th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose" id="allChoose"></th>
                    <th>角色名称</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody class="users_content">
                <%--循环数据--%>
                <c:choose>
                    <c:when test="${fn:length(data)==0}">
                        <tr>
                            <td colspan="4">暂无创建好的角色可供展示</td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${data}" var="role" varStatus="v">
                            <tr>
                                <td><input type="checkbox" name="ids" lay-skin="primary" lay-filter="aChoose" id="aChoose"></td>
                                <td>${role.roleName}</td>
                                <td>
                                    <input type="checkbox" lay-filter="OO" lay-skin="switch" data="${role.id}" lay-text="启用|停用" <c:if test="${role.flag==1}">checked</c:if> />
                                </td>
                                <td>
                                    <a title="编辑" href="javascript:void(0)" class="edit" data="${role.id}">
                                        <i class="layui-icon">&#xe642;</i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="javascript:void(0)" title="删除" class="delete" dataId="${role.id}" >
                                        <i class="layui-icon">&#xe640;</i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="javascript:void(0)" title="设置权限" class="permission" dataId="${role.id}" >
                                        <i class="layui-icon">&#xe716;</i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>
        <div id="page"></div>
            </div>
    </form>
</div>
</body>
<script>
    layui.use(['laydate','form','jquery','layer','laypage'], function(){
        layer = layui.layer;
        $ = layui.jquery;
        form = layui.form;
        laypage = layui.laypage;
        // 添加角色信息
        $(".usersAdd_btn").click(function () {
            layer.open({
                title:'添加角色信息',
                type:2,
                content:'system/role/toEdit',
                area:['500px','250px'],
            })
        });

        // 分页码-click
        laypage.render({
            elem:'page',
            count:parseInt(${page.total}),
            limit:parseInt(${page.pageSize}),
            curr:parseInt(${page.pageNum}),
            layout:['prev','page','next','skip'],
            jump:function(obj,first){
                if (!first){
                    $("#pageNum").val(obj.curr);
                    $("#form").submit();
                }
            }
        });

        // 模糊查询 ?条件
        $(".search_btn").click(function () {
            $("#form").submit();
        });

        // 全选
        form.on('checkbox(allChoose)', function(data){
            $("input[name='ids']").each(function () {
                this.checked = data.elem.checked;
            });
            form.render();
        });

        // 单选
        form.on('checkbox(aChoose)',function (data) {
            // 多选框的个数
            var allIds = $("input[name='ids']").length;
            // 被选中的多选框的长度
            var allIdsChecked = $("input[name='ids']:checked").length;
            if (parseInt(allIdsChecked)<allIds){
                // 不是全选中，取消√
                $("#allChoose")[0].checked = false;
            }else {
                // 是全选中，再√上
                $("#allChoose").get(0).checked = true;
            }
            form.render();
        });

        // 批量删除
        $(".batchDel").click(function () {
            if($("input[name='ids']:checked").length==0){
                layer.msg("你先选几个再点我删除么");
                return;
            }
            layer.confirm("是否确定删除"+$("input[name='ids']:checked").length+"个角色？",{icon:3,title:'Tip'},function () {
                var idsParam = $("#form").serialize();
                $.ajax({
                    url:'system/role/deletes',
                    type:'post',
                    dataType:'json',
                    data:idsParam,
                    success:function(ret){
                        if (ret.status){
                            layer.msg("删除成功！",{
                                offset:"t",
                                time:2000,
                                anim:4
                            },function () {
                                $("#form").submit();
                            })
                        }
                    }
                })
            })
        });

        // 单个删除
        $(".delete").click(function () {
            var id = $(this).attr("dataId");
            layer.confirm("是否确定删除这个角色？",{icon:3,title:'Tip'},function () {
                $.ajax({
                    url:'system/role/deletes',
                    type:'post',
                    dataType:'json',
                    data:{
                        ids:id
                    },
                    success:function(ret){
                        if (ret.status){
                            layer.msg("删除成功！",{
                                offset:"t",
                                time:2000,
                                anim:4
                            },function () {
                                $("#form").submit();
                            })
                        }
                    }
                })
            })

        });

        // 编辑
        $(".edit").click(function () {
            let id = $(this).attr("data");
            index = layer.open({
                title:'修改角色信息',
                type:2,
                content:'system/role/toEdit?id='+id,
                area:['500px','480px']
            });
        });

        // 更改角色状态
        form.on('switch(OO)',function (obj) {
            let id = $(this).attr("data");
            let flag = 0;
            if (obj.elem.checked){
                flag = 1;
            }
            $.ajax({
                url:'system/role/save?id='+id+'&flag='+flag,
                type:'get',
                dataType:'json',
                success:function (ret) {
                    if (!ret.status){
                        layer.msg("操作失败！")
                    }
                }
            })
        });

        // 更改角色权限
        $(".permission").click(function () {
            let roleid = $(this).attr("dataId");
            layer.open({
                title:'修改角色权限',
                type:2,
                content:'system/role/getPermission?roleId='+roleid,
                area:['500px','400px']
            })
        })


    })





</script>
</html>
