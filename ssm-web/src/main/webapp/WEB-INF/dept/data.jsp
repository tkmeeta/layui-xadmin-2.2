<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/26
  Time: 下午 9:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <title>用户总数--layui后台管理模板</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="layui/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
    <link rel="stylesheet" href="layui/css/user.css" media="all" />
</head>
<body class="childrenBody">
<blockquote class="layui-elem-quote news_search">
    <div class="layui-inline">
        <div class="layui-input-inline">
            <input type="text" value="" placeholder="请输入关键字" class="layui-input search_input">
        </div>
        <a class="layui-btn search_btn">查询</a>
    </div>
    <div class="layui-inline">
        <a class="layui-btn layui-btn-normal usersAdd_btn">添加部门</a>
    </div>
    <div class="layui-inline">
        <a class="layui-btn layui-btn-danger batchDel">批量删除</a>
    </div>
</blockquote>
<div class="layui-form users_list">
    <table class="layui-table">
        <colgroup>
            <%--固定值 --%>
            <col width="50">
                <%--剩余的--%>
            <col>
                <%--百分比--%>
            <col width="25%">
            <col width="8%">
            <col width="25%">
            <col width="15%">
        </colgroup>
        <thead>
        <tr>
            <th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose" id="allChoose"></th>
            <th>部门编号</th>
            <th>部门名称</th>
            <th>状态</th>
            <th>上级部门</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody class="users_content">
        <%--循环数据--%>
        </tbody>
    </table>
</div>
<div id="page"></div>
<script type="text/javascript" src="layui/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['layer','jquery','form'],function () {
        layer = layui.layer;
        $ = layui.jquery;
        form = layui.form;
        $(".usersAdd_btn").click(function () {
            layer.open({
                title:'添加部门信息',
                type:2,
                content:'dept/edit.jsp',
                area:['500px','329px'],
            })
        })

    })
</script>
</body>
</html>
