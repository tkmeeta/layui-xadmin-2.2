<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/28
  Time: 下午 1:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path=request.getContextPath()+"/";%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <title>添加部门</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="layui/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
    <div class="layui-form-item">
        <label class="layui-form-label">部门编号</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input userName" lay-verify="required" placeholder="请输入部门编号">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">部门名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input userEmail" lay-verify="email" placeholder="请输入部门名称">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-block">
            <input type="checkbox" name="zzz" lay-skin="switch" lay-text="开启|关闭">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">上级部门</label>
            <div class="layui-input-block">
                <select name="userGrade" class="userGrade" lay-filter="userGrade">
                    <option value="0">注册会员</option>
                    <option value="1">中级会员</option>
                    <option value="2">高级会员</option>
                    <option value="3">超级会员</option>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addUser">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="layui/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['form'],function () {
        form = layui.form;
        form.renderer;
    })
</script>
</body>
</html>
