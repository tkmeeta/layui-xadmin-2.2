<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/26
  Time: 下午 9:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>资源管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="z-tree/css/demo.css" type="text/css">
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="z-tree/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script type="text/javascript" src="z-tree/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="z-tree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="z-tree/js/jquery.ztree.exedit.js"></script>
    <link rel="stylesheet" href="layui/lib/layui/css/layui.css">
    <script src="layui/lib/layui/layui.js" charset="utf-8"></script>
</head>
<body>
<div class="layui-container" style="margin-top: 10px">
        <div class="layui-col-md3" style="height: 100%">
            <div id="treeDemo" class="ztree"></div>
        </div>
        <div class="layui-col-md9" style="border-left: 1px solid grey;">
            <%--表单--%>
                <form class="layui-form" id="form">
                    <input type="hidden" name="id" id="id">
                    <div class="layui-form-item">
                        <label class="layui-form-label">资源名称</label>
                        <div class="layui-input-block">
                            <input type="text" name="resourceName" id="resourceName" required  lay-verify="required" placeholder="请输入资源名称" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">父级资源</label>
                        <div class="layui-input-block">
                            <input type="hidden" name="parentId" id="parentId">
                            <input type="text" id="chooseParent" class="layui-input">
                            <div id="resourceTree" style="display: none;z-index:9;float:left;whidth:100%;height: 200px;border:1px solid #2E2D3C;background-color: #cccccc;overflow: auto;position: absolute;">
                                <div id="close" style="text-align: right"><a href="javascript:void(0)" title="关闭">x</a></div>
                                <div id="treeDemo2" class="ztree"></div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">资源图标</label>
                        <div class="layui-input-block">
                            <input type="text" name="icon" id="icon" placeholder="请输入资源icon" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">资源路径</label>
                        <div class="layui-input-block">
                            <input type="text" name="url" id="url" placeholder="请输入资源路径" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">资源类型</label>
                        <div class="layui-input-block">
                            <select name="resourceType" id="resourceType" lay-verify="required">
                                <option value="0">菜单</option>
                                <option value="1">按钮</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">权限标识</label>
                        <div class="layui-input-block">
                            <input type="text" name="resourcePermission" id="resourcePermission" placeholder="请输入资源标识" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-block">
                            <input type="text" name="resourceOrder" id="resourceOrder" placeholder="请输入排序序号" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" id="submit" type="button" lay-filter="formDemo">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
        </div>
</div>
</body>
<script type="text/javascript">
    // jquery的版本冲突-
    layui.use(['element','form','layer'],function () {
        element = layui.element;
        form = layui.form;

        // 提交表单
        $("#submit").click(function () {
            let param = $("form").serialize();
            $.ajax({
              url:'system/resource/save',
              type:'post',
              dataType:'json',
              data:param,
              success:function (ret) {
                  if(ret.status){
                      layer.msg('保存成功！',function () {
                          // 重新查询数据库，刷新树
                            createTree();
                            // 插入后清空表单的值
                            // $("#form").get(0).reset();
                          document.location = 'system/resource/data';
                      })
                  }else {layer.msg("操作失败")}
              }
            })
        });

        var setting = {
            data:{
                simpleData:{
                    enable: true,
                    idKey:"id",
                    pIdKey:"pid",
                    rootPId:0
                }
            },
            edit:{
                // 是否可处于编辑状态
                enable:true,
                showRemoveBtn:true,
                showRenameBtn:false,
                // removeTitle:deleteTreeNode 悬浮就会删除
            },
            callback:{
                // 点击节点的回调
                onClick:zTreeOnClick,
                onRemove:deleteTreeNode
            }
        };
        var setting2 = {
            data:{
                simpleData:{
                    enable: true,
                    idKey:"id",
                    pIdKey:"pid",
                    rootPId:0
                }
            },
            edit:{
                // 是否可处于编辑状态
                enable:false,
                showRemoveBtn:true,
                showRenameBtn:false,
                // removeTitle:deleteTreeNode 悬浮就会删除
            },
            callback:{
                // 点击节点的回调
                onClick:zTreeOnClick2,
            }
        };

        // 点击浮动div的节点，把名称传到input
        function zTreeOnClick2(event,treeId,treeNode) {
            $("#chooseParent").val(treeNode.name);
            $("#parentId").val(treeNode.id);
            $("#resourceTree").hide();
        }

        //  以 点击删除节点
        function deleteTreeNode(event,treeId,treeNode) {
                $.ajax({
                    url:'system/resource/deleteNode',
                    data:{
                        id:treeNode.id
                    },
                    dataType:'json',
                    type:'post',
                    success:function (ret) {
                        if (!ret.status){
                            layer.msg("删除失败",function () {
                                createTree();
                            });
                        }else {
                            layer.msg("删除成功",function () {
                                document.location = 'system/resource/data';
                            })
                        }
                    }
                })
        }


        // 点击节点 以 修改节点信息
        function zTreeOnClick(event,treeId,treeNode){
            $.ajax({
                url:'system/resource/getResource',
                dataType: 'json',
                type:'post',
                data:{
                    id:treeNode.id
                },
                success:function (ret) {
                    $("#id").val(ret.id)
                    $("#resourceName").val(ret.name);
                    $("#parentId").val(ret.pid);
                    $("#icon").val(ret.icon);
                    $("#url").val(ret.url);
                    $("#resourceType").val(ret.resourceType);
                    $("#resourcePermission").val(ret.resourcePermission);
                    $("#resourceOrder").val(ret.resourceOrder);
                    $("#chooseParent").val(ret.parent.name);
                }
            })
        }

        // 页面加载时--生成树and表单的必要信息
        $(document).ready(function () {
            createTree();
            // 边框
            $(".layui-col-md9").css("height",$(document).height());
        })
        // 异步请求数据库生成树的数据
        function createTree() {
            $.getJSON('system/resource/createTree?t='+new Date(),function (ret) {
                $.fn.zTree.init($("#treeDemo"),setting,ret); //位置，设置，json数据
                $.fn.zTree.init($("#treeDemo2"),setting2,ret); //位置，设置，json数据
                treeObj = $.fn.zTree.getZTreeObj("treeDemo");// treeID
                // 全部自动展开
                treeObj.expandAll(true);
            })
        }
        $("#chooseParent").focus(function () {
            $("#resourceTree").show();
        })
        $("#close").click(function () {
            $("#resourceTree").hide();

        })
    })
</script>
</html>
