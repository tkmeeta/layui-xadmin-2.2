<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/26
  Time: 下午 6:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html  class="x-admin-sm">
<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>后台登录-X-admin2.2</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" type="text/css" href="layui/css/font.css">
    <link rel="stylesheet" type="text/css" href="layui/css/login.css">
    <link rel="stylesheet" type="text/css" href="layui/css/xadmin.css">
    <script type="text/javascript" src="layui/js/jquery.min.js"></script>
    <script src="layui/lib/layui/layui.js" charset="utf-8"></script>
</head>
<body class="login-bg">

<div class="login layui-anim layui-anim-up">
    <div class="message">管理登录</div>
    <div id="darkbannerwrap"></div>

    <form method="post" class="layui-form" action="/login">
        <input name="loginAccount" placeholder="用户名"  type="text" lay-verify="required" class="layui-input" >
        <hr class="hr15">
        <input name="pwd" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
        <hr class="hr15">
        <div class="layui-form-item">
            <div class="layui-input-inline">
                <input type="text" name="code" required lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux" style="padding:0px 0!important"><img src="/verification" id="code" title="点击我，换一张"></div>

        </div>
        <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
        <hr class="hr20" >
    </form>
</div>

<script type="text/javascript">
        layui.use('form', function(){
            var form = layui.form;
            // 图形验证码
            $("#code").click(function(){
                $(this).attr("src","/verification?t="+new Date())
            });
            //监听提交
            form.on('submit(login)', function(data){
            });
        });
</script>
<!-- 底部结束 -->
</body>
</html>
