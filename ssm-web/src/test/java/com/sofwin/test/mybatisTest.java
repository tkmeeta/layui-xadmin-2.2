package com.sofwin.test;

import com.sofwin.pojo.SUser;
import com.sofwin.service.SUserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml", "classpath:applicationContext-dao.xml", "classpath:applicationContext-mvc.xml", "classpath:applicationContext-tx.xml"})
public class mybatisTest {
    @Autowired
    SUserService service;

    @Test
    public void test01(){
        SUser suser = new SUser();
        suser.setLoginAccount("admin");
        suser.setPwd("admin");
        suser.setRealName("管理员");
        suser.setLastLogin("2021-3-26 12:31");
        suser.setFlag(1);
        boolean b = service.saveUser(suser);
        Assert.assertEquals("true",String.valueOf(b));
    }
}
