package com.sofwin.test;

import com.sofwin.pojo.SUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/*测试控制反转*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class IocTest {
    @Resource(type = SUser.class)
    SUser suser;

    @Test
    public void test01(){
        Assert.assertNotNull(suser);
    }
}
